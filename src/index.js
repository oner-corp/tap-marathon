// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import {MiniApp} from 'aq-miniapp';
import View from './views/View';
import './index.css';

const minTime = 4;
const maxTime = 10;
const stepTime = 5;
const minTaps = 10;
const maxTaps = 15;

function generateTime(min, max, step) {
    return step * Math.round(min - 0.5 + Math.random() * (max - min + 1));
}

function generateTaps(min, max) {
  return  Math.round(min - 0.5 + Math.random() * (max - min + 1));
}

ReactDOM.render(
  <MiniApp
    join={View}
    data={{
      time: generateTime(minTime, maxTime, stepTime),
      taps: generateTaps(minTaps, maxTaps)
    }}
    devt={true}
  />,
  document.getElementById('root')
);
