import React, {Component} from 'react';
import type {Output} from '../Types';

import '../css/View3.css';
import {GameComponent} from "../components/GameComponent";

type Props = {
  output: Output
}

export class View3 extends Component {
  props: Props;

  render() {
    return (
      <div>
        <GameComponent/>
      </div>
    )
  }
}
