// @flow
import '../css/GameComponentCss.css';
import React, {Component} from 'react';
import * as Pixi from 'pixi.js';
import {sound as PixiSound} from 'pixi-sound';

import bgImg from '../../assets/bg.jpg'
import signImg from '../../assets/streetSign.png'
import rightFootImg from '../../assets/rightFoot.png';
import leftFootImg from '../../assets/leftFoot.png';
import speedometerImg from '../../assets/speedometer.png';
import finishImg from '../../assets/finishLine.png';
import streetLineImg from '../../assets/streetLine.png';
import houseImg from '../../assets/house.png';
import houseImg2 from '../../assets/house2.png';
import houseImg3 from '../../assets/house3.png';
import houseImg4 from '../../assets/house4.png';
import loseImg from '../../assets/loseLine.png';

import loseMp3 from '../../assets/lose_bg.mp3';
import tapMp3 from '../../assets/tap.mp3';
import winMp3 from '../../assets/win_bg.mp3';

const WHITE = 0xFFFFFF;
const RED = 0xFF0000;

const DEFAULT_FONT_FAMILY = 'Anton';

const START_BUTTON_TEXT = 'START';
const START_BUTTON_FONT_SIZE = 30;
const START_BUTTON_POSITION_RATIO = {x: 0.5, y: 0.85};
const START_BUTTON_RECTANGLE_ALPHA = 1;
const START_BUTTON_RECTANGLE_WIDTH = 150;
const START_BUTTON_RECTANGLE_HEIGHT = 40;
const START_BUTTON_RECTANGLE_RADIUS = 23;

const DISTANCE = 10;

const FOOTS_FILTER = new Pixi.filters.ColorMatrixFilter();
FOOTS_FILTER.greyscale(1);
const FOOT_SCALE = 0.2;
const MAX_DISTANCE_BETWEEN_FOOTS = 75;
const LEFT_FOOT_POSITION_X = 0.35;
const RIGHT_FOOT_POSITION_X = 0.65;
const LEADING_FOOT_POSITION_Y = 0.75;
const NOT_LEADING_FOOT_POSITION_Y = 0.85;
const FOOT_STEP_POSITION_Y = 50;

const TIMER_TEXT = 'Time:';
const TIMER_FONT_SIZE = 20;
const TIMER_TOP_OFFSET = 40;
const TIMER_RIGHT_OFFSET = 50;

const SPEEDOMETER_TOP_TEXT = 'FINISH LINE';
const SPEEDOMETER_TOP_BOTTOM_OFFSET = 40;
const SPEEDOMETER_TOP_RIGHT_OFFSET = 50;

const SPEEDOMETER_FONT_FAMILY = 'Orbitron';
const SPEEDOMETER_MEASURE = ' TAP';
const SPEEDOMETER_FONT_SIZE = 30;
const SPEEDOMETER_POSITION_RATIO = {x: 1, y: 0.25};
const SPEEDOMETER_RIGHT_OFFSET = 0;

const FINISH_POSITION_RATIO = {x: 0.5, y: 0.5};

const STREET_LINES_POSITION_RATIO = {x: 0.5, y: 0.45};
const STREET_LINES_SCALE_START = 0.1;
const STREET_LINES_SCALE_STEP = 0.1;
const STREET_LINES_POSITION_STEP = 25;

const HOUSES_POSITION_RATIO = {x: 0.65, y: 0.35};
const HOUSES_SCALE_START = 0.3;
const HOUSES_SCALE_STEP = 0.1;
const HOUSES_POSITION_STEP_Y = 10;
const HOUSES_POSITION_STEP_X = 20;
const HOUSES_DISTANCE = 10;

const SIGN_POSITION_RATIO = {x: 0.35, y: 0.4};
const SIGN_SCALE_START = 0.3;
const SIGN_SCALE_STEP = 0.1;
const SIGN_POSITION_STEP_Y = 15;
const SIGN_POSITION_STEP_X = 40;
const SIGN_TEXT = 'DISTANCE REACH';
const SIGN_FONT_SIZE = 15;
const SIGN_TEXT_TOP_OFFSET = -30;
const SIGN_DESC_TOP_OFFSET = 20;
const MEASURE_TEXT = ' Km';
const TAP_NUMBER_FOR_SIGN = 10;

const OPACITY_START = 0.01;
const OPACITY_STEP = 1;

const TOP = 0;
const CENTER = 0.5;
const RIGHT = 1;

const ANIMATIONS_STEPS = 10;

export type GameComponentProps = {
  time?: () => number,
  taps?: () => number
};
export type AnimationState = {
  timeStart?: Date,
  isStart?: boolean,
  isFootTapped?: boolean,
  isEnd?: boolean,
  isTapSoundPlaying?: boolean,
  isWinSoundPlaying?: boolean,
  isLoseSoundPlaying?: boolean,
  isStreetLinesDrawed?: boolean,
  isHousesDrawed?: boolean,
  isSignDrawed?: boolean,
  animateStreetLines?: boolean,
  animateSign?: boolean,
  animateHouses?: boolean,
  currentStreetLinesAnimationStep?: number,
  currentSignAnimationStep?: number,
  currentHousesAnimationStep?: number,
  isAlphaIsFull?: boolean,
  isLeftFootLead?: boolean,
  animateFoots?: boolean,
  currentFootAnimationSteps?: number,
  totalHouseAnimationSteps?: number,
  totalSignAnimationSteps?: number,
  totalStreetLinesAnimationSteps?: number,
  totalFootAnimationSteps?: number
}

function randomInteger(min, max) {
  var rand = min - 0.5 + Math.random() * (max - min + 1)
  rand = Math.round(rand);
  return rand;
}
export class GameComponent extends Component {
  animationState: AnimationState;
  props: GameComponentProps;

  constructor(props: GameComponentProps) {
    super(props);

    this.app = null;
    this.gameCanvas = null;
    this.bg = null;
    this.gameContainer = null;
    this.finishContainer = null;
    this.loseContainer = null;
    this.startContainer = null;
    this.streetLinesContainer = null;
    this.housesContainer = null;

    this.leftFoot = null;
    this.rightFoot = null;
    this.speedometer = null;
    this.timer = null;
  }

  componentDidMount() {
    Pixi.settings.SCALE_MODE = Pixi.SCALE_MODES.LINEAR;

    this.app = new Pixi.Application(window.innerWidth, window.innerHeight,
      {antialias: true});
    this.gameCanvas.appendChild(this.app.view);

    this.createBackground();

    this.gameContainer = new Pixi.Container();
    this.streetLinesContainer = new Pixi.Container();
    this.housesContainer = new Pixi.Container();
    this.finishContainer = new Pixi.Container();
    this.loseContainer = new Pixi.Container();
    this.startContainer = new Pixi.Container();

    this.app.stage.addChild(this.streetLinesContainer);
    this.app.stage.addChild(this.housesContainer);
    this.app.stage.addChild(this.startContainer);
    this.app.stage.addChild(this.gameContainer);
    this.app.stage.addChild(this.loseContainer);
    this.app.stage.addChild(this.finishContainer);

    this.streetLinesContainer.visible = true;
    this.housesContainer.visible = true;
    this.finishContainer.visible = false;
    this.loseContainer.visible = false;
    this.gameContainer.visible = false;

    this.gameState = {
      updateBgScale: true,
      updateLoseScale: true,
      updateFinishScale: true,
      timeStart: this.props.calculateTime(),
      tapCounter: 0,
      tapTarget: this.props.calculateTaps(),
      isLeftFootActive: false,
      isRightFootActive: false,
      housesArray: [],
      streetLinesArray: []
    };
    this.gameState.timeLeft = this.gameState.timeStart;
    this.animationState = {};

    this.loadSounds();
    this.createStartButton();
    this.createSpeedometer();
    this.createTimer();
    this.createFinishLine();
    this.createLoseLine();
    this.createAllStreetLines();
    this.createAllHouses();
    this.createFoots();

    this.app.ticker.add((d) => this.tickHandler(d));

    this.app.start();
    this.resetGame();
  }

  componentWillUnmount() {
    this.app.stop();
  }

  static createTextStyle(color, fontFamily, fontSize) {
    return new Pixi.TextStyle({
      fill: color,
      fontFamily: fontFamily,
      fontSize: fontSize
    });
  }

  static setPosition(obj, relatedObj, ratio) {
    obj.x = ratio.x * relatedObj.width;
    obj.y = ratio.y * relatedObj.height;
  }

  static setAnchorAndPosition(obj, relatedObj, ratio) {
    obj.anchor.set(CENTER);
    GameComponent.setPosition(obj, relatedObj, ratio);
  }

  createBackground() {
    this.bg = Pixi.Sprite.fromImage(bgImg);
    this.app.stage.addChild(this.bg);
  }

  createSignAndText() {
    const signContainer = new Pixi.Container();
    signContainer.addChild(this.createSign());
    signContainer.addChild(this.createSignText());

    signContainer.scale.x = SIGN_SCALE_START;
    signContainer.scale.y = SIGN_SCALE_START;
    GameComponent.setPosition(signContainer, this.app.screen, SIGN_POSITION_RATIO);
    signContainer.alpha = OPACITY_START;

    this.gameContainer.addChild(signContainer);
    this.gameState.sign = signContainer;
  }
  createSign() {
    const sign = Pixi.Sprite.fromImage(signImg);
    sign.anchor.set(CENTER);
    //GameComponent.setAnchorAndPosition(sign, this.app.screen, SIGN_POSITION_RATIO);
    return sign;
  }
  createSignText() {
    const signText = DISTANCE + MEASURE_TEXT;
    const nextDistanceStyle = GameComponent.createTextStyle(WHITE, DEFAULT_FONT_FAMILY, SIGN_FONT_SIZE);

    const signTextContainer = new Pixi.Container();
    //GameComponent.setPosition(signTextContainer, this.app.screen, SIGN_POSITION_RATIO);

    const pixiSignText = new Pixi.Text(signText, nextDistanceStyle);
    const pixiSignDesc = new Pixi.Text(SIGN_TEXT, nextDistanceStyle);
    pixiSignText.anchor.set(CENTER);
    pixiSignDesc.anchor.set(CENTER);
    pixiSignText.y = SIGN_TEXT_TOP_OFFSET;
    pixiSignDesc.y = SIGN_DESC_TOP_OFFSET + SIGN_TEXT_TOP_OFFSET;

    signTextContainer.addChild(pixiSignText);
    signTextContainer.addChild(pixiSignDesc);
    return signTextContainer;
  }

  moveSign() {
    if (this.animationState.animateSign) {
      return;
    }
    this.animationState.animateSign = true;
    this.animationState.totalSignAnimationSteps = ANIMATIONS_STEPS;
    this.animationState.currentSignAnimationStep = 1;
  };
  handleSignAnimation() {
    if (this.animationState.currentSignAnimationStep > this.animationState.totalSignAnimationSteps) {
    this.animationState.animateSign = false;
    this.animationState.currentSignAnimationStep = 0;
    this.animationState.totalSignAnimationSteps = 0;
    return;
  }

    this.animationState.currentSignAnimationStep += 1;
    const totalSteps = this.animationState.totalSignAnimationSteps;

    const ratio = 1;
    this.gameState.sign.scale.x += (SIGN_SCALE_STEP / totalSteps) * ratio;
    this.gameState.sign.scale.y += (SIGN_SCALE_STEP / totalSteps) * ratio;
    this.gameState.sign.position.y += (SIGN_POSITION_STEP_Y / totalSteps) * ratio;
    this.gameState.sign.position.x -= (SIGN_POSITION_STEP_X / totalSteps) * ratio;
    this.gameState.sign.alpha += OPACITY_STEP / totalSteps;

    if (this.gameState.sign.position.y > this.app.screen.height) {
      this.gameContainer.removeChild(this.gameState.sign);
      this.gameState.sign = null;
      this.animationState.isSignDrawed = true;
    }
  };

  createStartButton() {
    const text = GameComponent.createStartButtonText();
    const rectangle = GameComponent.createStartButtonRectangle();

    const startButtonContainer = new Pixi.Container();
    GameComponent.setPosition(startButtonContainer, this.app.screen, START_BUTTON_POSITION_RATIO);
    startButtonContainer.addChild(rectangle);
    startButtonContainer.addChild(text);

    startButtonContainer.interactive = true;
    startButtonContainer.buttonMode = true;
    startButtonContainer.on('pointerup', () => this.startGame());

    this.startContainer.addChild(startButtonContainer);
  }

  static createStartButtonText() {
    const startTextStyle = GameComponent.createTextStyle(WHITE, DEFAULT_FONT_FAMILY, START_BUTTON_FONT_SIZE);
    const startText = new Pixi.Text(START_BUTTON_TEXT, startTextStyle);
    startText.anchor.set(CENTER);

    return startText;
  }

  static createStartButtonRectangle() {
    let container = new Pixi.Graphics();
    container.beginFill(RED, START_BUTTON_RECTANGLE_ALPHA);
    container.drawRoundedRect(0, 0,
      START_BUTTON_RECTANGLE_WIDTH,
      START_BUTTON_RECTANGLE_HEIGHT,
      START_BUTTON_RECTANGLE_RADIUS
    );
    container.endFill();

    container.pivot.set(container.width / 2, container.height / 2);
    return container;
  }

  createFoots() {
    this.createLeftFoot();
    this.createRightFoot();

    const horizontalDistance = this.rightFoot.x - this.leftFoot.x;
    if (horizontalDistance > MAX_DISTANCE_BETWEEN_FOOTS) {
      this.rightFoot.x -= (horizontalDistance - MAX_DISTANCE_BETWEEN_FOOTS) / 2;

      this.leftFoot.x += (horizontalDistance - MAX_DISTANCE_BETWEEN_FOOTS) / 2;
    }

  }

  createLeftFoot() {
    this.leftFoot = Pixi.Sprite.fromImage(leftFootImg);
    this.leftFoot.scale.x = FOOT_SCALE;
    this.leftFoot.scale.y = FOOT_SCALE;

    const position = {x: LEFT_FOOT_POSITION_X, y: LEADING_FOOT_POSITION_Y};

    GameComponent.setAnchorAndPosition(this.leftFoot, this.app.screen, position);

    this.leftFoot.interactive = true;
    this.leftFoot.buttonMode = true;
    this.leftFoot.on('pointerup', () => this.leftFootHandler());

    this.gameContainer.addChild(this.leftFoot);
  }

  createRightFoot() {
    this.rightFoot = Pixi.Sprite.fromImage(rightFootImg);
    this.rightFoot.scale.x = FOOT_SCALE;
    this.rightFoot.scale.y = FOOT_SCALE;


    const position = {x: RIGHT_FOOT_POSITION_X, y: NOT_LEADING_FOOT_POSITION_Y};
    GameComponent.setAnchorAndPosition(this.rightFoot, this.app.screen, position);

    this.rightFoot.interactive = true;
    this.rightFoot.buttonMode = true;
    this.rightFoot.on('pointerup', () => this.rightFootHandler());

    this.gameContainer.addChild(this.rightFoot);
  }

  loadSounds() {
    this.winSound = PixiSound.Sound.from({url: winMp3, preload: true});
    this.loseSound = PixiSound.Sound.from({url: loseMp3, preload: true});
    this.tapSound = PixiSound.Sound.from({url: tapMp3, preload: true});
  }

  createTimer() {
    const timeTextStyle = GameComponent.createTextStyle(WHITE, DEFAULT_FONT_FAMILY, TIMER_FONT_SIZE);

    this.timer = new Pixi.Text(this.gameState.timeLeft + '.00', timeTextStyle);
    const timeLeftText = new Pixi.Text(TIMER_TEXT, timeTextStyle);

    this.timer.anchor.set(CENTER);
    this.timer.x += this.timer.width / 2;

    timeLeftText.anchor.set(CENTER);
    timeLeftText.x -= timeLeftText.width / 2;

    const time = new Pixi.Text();
    time.addChild(this.timer, timeLeftText);

    GameComponent.setAnchorAndPosition(time, this.app.screen, SPEEDOMETER_POSITION_RATIO);
    time.x -= TIMER_RIGHT_OFFSET;
    time.y += TIMER_TOP_OFFSET;
    this.gameContainer.addChild(time);
  }

  createSpeedometer() {
    const speedometerImg = GameComponent.createSpeedometerImg();
    const speedometerText = this.createSpeedometerText();

    speedometerImg.anchor.set(RIGHT, CENTER);
    speedometerText.anchor.set(RIGHT, CENTER);
    GameComponent.setPosition(speedometerImg, this.app.screen, SPEEDOMETER_POSITION_RATIO);
    GameComponent.setPosition(speedometerText, this.app.screen, SPEEDOMETER_POSITION_RATIO);

    speedometerText.x -= SPEEDOMETER_RIGHT_OFFSET;

    this.gameContainer.addChild(speedometerImg);
    this.gameContainer.addChild(speedometerText);

    this.createSpeedometerTopText();
  }

  static createSpeedometerImg() {
    return Pixi.Sprite.fromImage(speedometerImg);
  }

  createSpeedometerText() {
    const speedometerStyle = GameComponent.createTextStyle(WHITE, SPEEDOMETER_FONT_FAMILY, SPEEDOMETER_FONT_SIZE);
    this.speedometer = new Pixi.Text('0', speedometerStyle);
    const measure = new Pixi.Text(SPEEDOMETER_MEASURE, speedometerStyle);
    this.speedometer.anchor.set(RIGHT, CENTER);
    measure.anchor.set(RIGHT, CENTER);

    this.speedometer.x -= measure.width;

    const speedometerText = new Pixi.Text();
    speedometerText.addChild(this.speedometer, measure);

    return speedometerText;
  }
  createSpeedometerTopText() {
    const textStyle = GameComponent.createTextStyle(WHITE, DEFAULT_FONT_FAMILY, TIMER_FONT_SIZE);
    const speedometerTopText = new Pixi.Text(SPEEDOMETER_TOP_TEXT, textStyle);

    GameComponent.setAnchorAndPosition(speedometerTopText, this.app.screen, SPEEDOMETER_POSITION_RATIO);
    speedometerTopText.x -= SPEEDOMETER_TOP_RIGHT_OFFSET;
    speedometerTopText.y -= SPEEDOMETER_TOP_BOTTOM_OFFSET;
    this.gameContainer.addChild(speedometerTopText);
  }

  createFinishLine() {
    this.finish = Pixi.Sprite.fromImage(finishImg);
    GameComponent.setAnchorAndPosition(this.finish, this.app.screen, FINISH_POSITION_RATIO);
    this.finishContainer.addChild(this.finish);
  }

  createLoseLine() {
    this.lose = Pixi.Sprite.fromImage(loseImg);
    GameComponent.setAnchorAndPosition(this.lose, this.app.screen, FINISH_POSITION_RATIO);
    this.loseContainer.addChild(this.lose);
  }

  startGame() {
    this.startContainer.visible = false;
    this.gameContainer.visible = true;

    this.animationState = {};

    this.animationState.timeStart = new Date();
    this.animationState.isStart = true;

    this.gameState.updateBgScale = false;
    this.gameState.updateFinishScale = false;
    this.gameState.updateLoseScale = false;
  }

  resetGame() {
    if (this.animationState.isLoseSoundPlaying) {
      this.loseSound.stop();
    }
    if (this.animationState.isWinSoundPlaying) {
      this.winSound.stop();
    }

    this.finishContainer.visible = false;
    this.loseContainer.visible = false;
    this.gameContainer.visible = false;

    this.gameState = {
      updateBgScale: true,
      updateFinishScale: true,
      updateLoseScale: true,
      timeStart: this.props.calculateTime(),
      tapTarget: this.props.calculateTaps(),
      streetLinesArray: this.gameState.streetLinesArray,
      housesArray: this.gameState.housesArray,
      tapCounter: 0,
      isLeftFootActive: false,
      isRightFootActive: false,
      requiredStreetLinesCount: this.gameState.requiredStreetLinesCount,
      requiredHousesCount: this.gameState.requiredHousesCount
    };
    this.gameState.timeLeft = this.gameState.timeStart;
    this.animationState = {
      timeStart: new Date()
    };
  }

  tickHandler(delta) {
    if (this.gameState.updateBgScale && this.bg.height > 1) {
      this.updateBgScale();
      this.gameState.updateBgScale = false;
    }

    if (this.gameState.updateFinishScale && this.finish.height > 1) {
      this.updateFinishScale();
      this.gameState.updateFinishScale = false;
    }

    if (this.gameState.updateLoseScale && this.lose.height > 1) {
      this.updateLoseScale();
      this.gameState.updateLoseScale = false;
    }

    if (this.gameState.timeLeft > 0 && this.animationState.isStart && !this.animationState.isEnd) {
      this.timer.text = this.gameState.timeLeft.toFixed(2);

      this.gameState.timeLeft = this.gameState.timeStart
        - GameComponent.getDeltaInSeconds(this.animationState.timeStart);

      if (this.gameState.timeLeft <= 0) {
        this.gameState.isTimeOut = true;
        this.timer.text = 0;
        this.timeLeftHandler();
      }
    }

    if (this.animationState.animateStreetLines) {
      this.handleStreetLinesAnimation();
    }

    if (this.animationState.animateSign) {
      this.handleSignAnimation();
    }

    if (this.animationState.animateHouses) {
      this.handleHousesAnimation();
    }

    if (this.animationState.animateFoots) {
      this.handleFootsAnimation();
    }
  }

  handleStreetLinesAnimation() {
    if (this.animationState.currentStreetLinesAnimationStep > this.animationState.totalStreetLinesAnimationSteps) {
      this.createStreetLine();
      this.animationState.animateStreetLines = false;
      this.animationState.currentStreetLinesAnimationStep = 0;
      this.animationState.totalStreetLinesAnimationSteps = 0;
      return;
    }

    this.animationState.currentStreetLinesAnimationStep += 1;
    const totalSteps = this.animationState.totalStreetLinesAnimationSteps;

    const that = this;
    this.gameState.streetLinesArray.forEach(function (item, i, arr) {
      const lineRatio = arr.length - i;
      item.scale.x += (STREET_LINES_SCALE_STEP / totalSteps) * lineRatio;
      item.scale.y += (STREET_LINES_SCALE_STEP / totalSteps) * lineRatio;
      item.position.y += (STREET_LINES_POSITION_STEP / totalSteps) * lineRatio;
      item.alpha += OPACITY_STEP / totalSteps;

      if (item.position.y > that.app.screen.height) {
        arr.shift();
        that.gameContainer.removeChild(item);
        that.animationState.isStreetLinesDrawed = true;
      }
    });
  }

  handleFootsAnimation() {
    if (this.animationState.currentFootAnimationSteps > this.animationState.totalFootAnimationSteps) {
      this.animationState.animateFoots = false;
      this.animationState.currentFootAnimationSteps = 0;
      this.animationState.totalFootAnimationSteps = 0;
      this.animationState.isLeftFootLead = !this.animationState.isLeftFootLead;
      return;
    }

    this.animationState.currentFootAnimationSteps += 1;
    if (this.animationState.isLeftFootLead) {
      this.moveFoot(this.leftFoot, this.rightFoot);
    } else {
      this.moveFoot(this.rightFoot, this.leftFoot);
    }

  }

  moveFoot(leadFoot, notLeadFoot) {
    const offset = FOOT_STEP_POSITION_Y / this.animationState.totalFootAnimationSteps;
    leadFoot.position.y -= offset;
    notLeadFoot.position.y += offset;
  }

  handleHousesAnimation() {
    if (this.animationState.currentHousesAnimationStep > this.animationState.totalHouseAnimationSteps) {
      this.createHouse();
      this.animationState.animateHouses = false;
      this.animationState.currentHousesAnimationStep = 0;
      this.animationState.totalHouseAnimationSteps = 0;
      return;
    }

    this.animationState.currentHousesAnimationStep += 1;
    const totalSteps = this.animationState.totalHouseAnimationSteps;

    const that = this;
    this.gameState.housesArray.forEach(function (item, i, arr) {
      const houseRatio = arr.length - i;
      item.scale.x += (HOUSES_SCALE_STEP / totalSteps) * houseRatio;
      item.scale.y += (HOUSES_SCALE_STEP / totalSteps) * houseRatio;
      item.position.y -= (HOUSES_POSITION_STEP_Y / totalSteps) * houseRatio;
      item.position.x += (HOUSES_POSITION_STEP_X / totalSteps) * houseRatio;
      item.alpha += OPACITY_STEP / totalSteps;

      if (item.position.x - item.width > that.app.screen.width) {
        arr.shift();
        that.gameContainer.removeChild(item);
        that.animationState.isHousesDrawed = true;
      }
    });
  }

  updateBgScale() {
    const widthScale = this.app.screen.width / this.bg.width;
    const heightScale = this.app.screen.height / this.bg.height;

    if (widthScale > 1 || heightScale > 1) {
      const scale = Math.max(widthScale, heightScale);
      this.bg.scale.x = scale;
      this.bg.scale.y = scale;
    }

    if (this.app.screen.width < this.bg.width) {
      this.bg.x = (this.app.screen.width - this.bg.width) / 2;
    }
    if (this.app.screen.height < this.bg.height) {
      this.bg.y = (this.app.screen.height - this.bg.height) / 2;
    }
  }

  updateFinishScale() {
    const finishScale = this.app.screen.width / this.finish.width;
    this.finish.scale.x = finishScale;
    this.finish.scale.y = finishScale;
  }

  updateLoseScale() {
    const loseScale = this.app.screen.width / this.lose.width;
    this.lose.scale.x = loseScale;
    this.lose.scale.y = loseScale;
  }

  rightFootHandler() {
    this.rightFoot.filters = [FOOTS_FILTER];
    this.gameState.isRightFootActive = false;

    if (!this.gameState.isLeftFootActive) {
      this.gameState.isLeftFootActive = true;
      this.leftFoot.filters = [];
      this.stepHandler();
    }

  }

  leftFootHandler() {
    this.leftFoot.filters = [FOOTS_FILTER];
    this.gameState.isLeftFootActive = false;

    if (!this.gameState.isRightFootActive) {
      this.gameState.isRightFootActive = true;
      this.rightFoot.filters = [];
      this.stepHandler();
    }

  }

  stepHandler() {
    if (!this.animationState.animateFoots) {
      this.animationState.currentFootAnimationSteps = 0;
      this.animationState.totalFootAnimationSteps = ANIMATIONS_STEPS;
      this.animationState.animateFoots = true;
    }

    this.speedometer.text = +this.speedometer.text + 1;
    this.animationState.isTapSoundPlaying = true;
    const tapSound = this.tapSound.play();
    tapSound.on('end', () => this.animationState.isTapSoundPlaying = false);

    this.gameState.tapCounter++;
    this.moveStreetLines();
    this.moveHouses();
    if (this.gameState.sign) {
      this.moveSign();
    }
    this.animationState.isFootTapped = true;

    if (TAP_NUMBER_FOR_SIGN - this.gameState.requiredHousesCount - this.gameState.tapCounter === 0) {
      this.createSignAndText();
    }

    if (this.gameState.tapCounter >= this.gameState.tapTarget) {
      this.gameState.isFinish = true;
      this.finishHandler();
    }
  }

  timeLeftHandler() {
    this.animationState.isLoseSoundPlaying = true;
    const loseSound = this.loseSound.play();
    loseSound.on('end', () => this.animationState.isWinSoundPlaying = false);

    this.loseContainer.visible = true;
    this.endOfGameHandler();
  }

  finishHandler() {
    this.animationState.isWinSoundPlaying = true;
    const winSound = this.winSound.play();
    winSound.on('end', () => this.animationState.isWinSoundPlaying = false);

    this.endOfGameHandler();
    this.finishContainer.visible = true;
  }

  endOfGameHandler() {
    this.rightFoot.interactive = false;
    this.leftFoot.interactive = false;
    this.animationState.isEnd = true;
  }

  createStreetLine(opacity) {
    const streetLine = Pixi.Sprite.fromImage(streetLineImg);

    streetLine.scale.set(STREET_LINES_SCALE_START);
    streetLine.anchor.set(CENTER, TOP);
    GameComponent.setPosition(streetLine, this.app.screen, STREET_LINES_POSITION_RATIO);
    streetLine.alpha = opacity || OPACITY_START;

    this.gameState.streetLinesArray.push(streetLine);

    this.streetLinesContainer.addChildAt(streetLine, 0);
  }

  createAllStreetLines() {
    this.createStreetLine(1);
    while (!this.animationState.isStreetLinesDrawed) {
      const that = this;

      this.gameState.streetLinesArray.forEach(function (item, i, arr) {
        item.scale.x += STREET_LINES_SCALE_STEP * (arr.length - i);
        item.scale.y += STREET_LINES_SCALE_STEP * (arr.length - i);
        item.position.y += STREET_LINES_POSITION_STEP * (arr.length - i);

        if (item.position.y > that.app.screen.height) {
          that.animationState.isStreetLinesDrawed = true;
        }
      });

      this.createStreetLine(1);
    }
    this.gameState.requiredStreetLinesCount = this.gameState.streetLinesArray.length;
  }

  moveStreetLines() {
    if (this.animationState.animateStreetLines) {
      return;
    }
    this.animationState.animateStreetLines = true;
    this.animationState.totalStreetLinesAnimationSteps = ANIMATIONS_STEPS;
    this.animationState.currentStreetLinesAnimationStep = 1;
  }

  createAllHouses() {
    this.createHouse(1);

    while (!this.animationState.isHousesDrawed) {
      const that = this;

      this.gameState.housesArray.forEach(function (item, i, arr) {
        item.scale.x += HOUSES_SCALE_STEP * (arr.length - i+1);
        item.scale.y += HOUSES_SCALE_STEP * (arr.length - i+1);
        item.position.y -= HOUSES_POSITION_STEP_Y * (arr.length - i+1);
        item.position.x += HOUSES_POSITION_STEP_X * (arr.length - i+1);

        if (item.position.x - item.width > that.app.screen.width) {
          that.animationState.isHousesDrawed = true;
        }
      });

      this.createHouse(1);
    }
    this.gameState.requiredHousesCount = this.gameState.housesArray.length;

  }

  createHouse(opacity) {
    const num = randomInteger(0,3);
    const house = num === 0 ? Pixi.Sprite.fromImage(houseImg)
      : num === 1 ? Pixi.Sprite.fromImage(houseImg2)
      : num === 2 ? Pixi.Sprite.fromImage(houseImg3)
      : Pixi.Sprite.fromImage(houseImg4);

    house.scale.set(HOUSES_SCALE_START);
    house.anchor.set(CENTER, TOP);
    GameComponent.setPosition(house, this.app.screen, HOUSES_POSITION_RATIO);
    house.alpha = opacity || OPACITY_START;

    const randomDistance = randomInteger(0, 8);
    house.scale.x += (HOUSES_SCALE_STEP / HOUSES_DISTANCE) * randomDistance;
    house.scale.y += (HOUSES_SCALE_STEP / HOUSES_DISTANCE) * randomDistance;
    house.position.y -= (HOUSES_POSITION_STEP_Y / HOUSES_DISTANCE) * randomDistance;
    house.position.x += (HOUSES_POSITION_STEP_X / HOUSES_DISTANCE) * randomDistance;

    this.gameState.housesArray.push(house);

    this.housesContainer.addChildAt(house, 0);
  }

  moveHouses() {
    if (this.animationState.animateHouses) {
      return;
    }
    this.animationState.totalHouseAnimationSteps = ANIMATIONS_STEPS;
    this.animationState.animateHouses = true;
    this.animationState.currentHousesAnimationStep = 1;
  }

  static getDeltaInSeconds(time) {
    const currentTime = new Date().getTime();
    return (currentTime - time.getTime()) / 1000;
  }

  render() {
    const component = this;
    return (
      <div ref={(thisDiv) => {
        component.gameCanvas = thisDiv
      }}/>
    );
  }
}


